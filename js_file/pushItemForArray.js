var numberArray = [];
var positiveNumberArray = [];
var numberArrayForChange = [];
var negativeNumberArray = [];

document
  .getElementById("push-number-btn")
  .addEventListener("click", pushItemForArray);

function pushItemForArray() {
  var numberValue = document.getElementById("number-input").value * 1;
  function pushItemForNumberArray() {
    numberArray.push(numberValue);
    document.getElementById("p-show-array").innerHTML = numberArray.join(", ");
    // console.log(numberArray);
  }
  pushItemForNumberArray();
  function pushItemForPositiveNumberArray() {
    if (numberValue > 0) {
      positiveNumberArray.push(numberValue);
      // console.log(positiveNumberArray);
    }
  }
  pushItemForPositiveNumberArray();
  function pushItemForNumberArrayForChange() {
    numberArrayForChange.push(numberValue);
  }
  pushItemForNumberArrayForChange();
  function pushItemForNegativeNumberArray() {
    if (numberValue < 0) {
      negativeNumberArray.push(numberValue);
      console.log(negativeNumberArray);
    }
  }
  pushItemForNegativeNumberArray();
  document.getElementById("number-input").value = "";
}
