document
  .getElementById("do-task-03-btn")
  .addEventListener("click", function () {
    var minInNumberArray = numberArray[0];
    for (i = 0; i < numberArray.length; i++) {
      if (minInNumberArray > numberArray[i]) {
        minInNumberArray = numberArray[i];
      }
    }
    document.getElementById(
      "p-show-result-task-03"
    ).innerHTML = `<b>Số nhỏ nhất trong mảng:</b> ${minInNumberArray}`;
  });
