var task_09_numberArray = [];
var task_09_integerArray = [];
document
  .getElementById("push-number-for-task-09-btn")
  .addEventListener("click", function () {
    var numberValue = document.getElementById("number-input-task-09").value * 1;
    task_09_numberArray.push(numberValue);
    if (Number.isInteger(numberValue)) {
      task_09_integerArray.push(numberValue);
    }
    document.getElementById("p-show-task-09-array").innerHTML =
      task_09_numberArray.join(", ");
    document.getElementById("number-input-task-09").value = "";
  });

document
  .getElementById("do-task-09-btn")
  .addEventListener("click", function () {
    document.getElementById("p-show-result-task-09").innerHTML =
      task_09_integerArray.length;
    console.log(task_09_integerArray);
  });
