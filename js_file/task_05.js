document
  .getElementById("do-task-05-btn")
  .addEventListener("click", function () {
    var evenNumber = null;
    for (i = 0; i < numberArray.length; i++) {
      var remainderNumber = numberArray[i] % 2;
      if (remainderNumber == 0) {
        evenNumber = numberArray[i];
      }
    }
    // document.getElementById(
    //   "p-show-result-task-05"
    // ).innerHTML = `<b>Vị trí cuối cùng của số chẵn:</b> ${numberArray.lastIndexOf(
    //   evenNumber
    // )}`;
    var resultInnerEl = document.getElementById("p-show-result-task-05");
    if (numberArray.lastIndexOf(evenNumber) > -1) {
      resultInnerEl.innerHTML = `<b>Số chẵn cuối cùng trong mảng:</b> ${evenNumber}`;
    } else {
      resultInnerEl.innerHTML = `Trong mảng không tồn tại số chẵn`;
    }
  });
