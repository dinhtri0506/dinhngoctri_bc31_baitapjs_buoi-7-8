document
  .getElementById("do-task-07-btn")
  .addEventListener("click", function () {
    var numberArrayBeforeSort = numberArray.slice(0);
    var numberArrayAfterSort = [];
    var minNumber = numberArrayBeforeSort[0];
    var positionOfMinNumber = null;
    for (i = 0; i < numberArray.length; i++) {
      for (j = 0; j < numberArrayBeforeSort.length; j++) {
        if (minNumber > numberArrayBeforeSort[j]) {
          minNumber = numberArrayBeforeSort[j];
        }
      }
      numberArrayAfterSort.push(minNumber);
      positionOfMinNumber = numberArrayBeforeSort.indexOf(minNumber);
      numberArrayBeforeSort.splice(positionOfMinNumber, 1);
      minNumber = numberArrayBeforeSort[0];
    }
    document.getElementById(
      "p-show-result-task-07"
    ).innerHTML = `<b>Mảng sau khi được sắp xếp:</b> ${numberArrayAfterSort.join(
      ", "
    )}`;
  });
