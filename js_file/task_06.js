document
  .getElementById("do-task-06-btn")
  .addEventListener("click", function () {
    var position_01 = parseInt(
      document.getElementById("position-01").value * 1
    );
    var position_02 = parseInt(
      document.getElementById("position-02").value * 1
    );

    var change_01 = numberArrayForChange[position_01];
    var change_02 = numberArrayForChange[position_02];

    numberArrayForChange[position_01] = change_02;
    numberArrayForChange[position_02] = change_01;

    document.getElementById(
      "p-show-result-task-06"
    ).innerHTML = `<b>Mảng sau khi đổi:</b>
      ${numberArrayForChange.join(", ")}`;
  });
