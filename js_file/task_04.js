document
  .getElementById("do-task-04-btn")
  .addEventListener("click", function () {
    var minInPositiveNumberArray = positiveNumberArray[0];
    for (i = 0; i < positiveNumberArray.length; i++) {
      if (minInPositiveNumberArray > positiveNumberArray[i]) {
        minInPositiveNumberArray = positiveNumberArray[i];
      }
    }
    document.getElementById(
      "p-show-result-task-04"
    ).innerHTML = `<b>Số dương nhỏ nhất trong mảng:</b> ${minInPositiveNumberArray}`;
  });
