document
  .getElementById("do-task-08-btn")
  .addEventListener("click", getPositionOfFirstPrimeNumberInArray);
const primeNumberArray = [2, 3, 5, 7];
function getPositionOfFirstPrimeNumberInArray() {
  var primeNumber = null;
  var jLoop_01 = 0;
  var jLoop_02 = 0;
  function findFirstPrimeNumberInArray() {
    for (i = 0; i < numberArray.length; i++) {
      // Kiểm tra numberArray[i] có phải số tự nhiên hay không?
      if (numberArray[i] <= 1 || numberArray[i] % 1 != 0) {
        continue;
      } else {
        var conditionToStop = parseInt(Math.sqrt(numberArray[i]));
        if (conditionToStop < 2) {
          return (primeNumber = numberArray[i]);
        } else {
          for (j = 2; j <= conditionToStop; j++) {
            ++jLoop_01;
            if (j != 2 && j != conditionToStop && j % 2 == 0) {
              continue;
            } else {
              ++jLoop_02;
              if (numberArray[i] % j == 0) {
                break;
              } else {
                if (j != conditionToStop) {
                  continue;
                } else {
                  return (primeNumber = numberArray[i]);
                }
              }
            }
          }
        }
      }
    }
  }
  findFirstPrimeNumberInArray();
  console.log({ primeNumber });
  console.log({ jLoop_01, jLoop_02 });
  var positionOfFirstPrimeNumberInArray = numberArray.indexOf(primeNumber);
  var resultInnerEl = document.getElementById("p-show-result-task-08");
  if (positionOfFirstPrimeNumberInArray > -1) {
    resultInnerEl.innerHTML = `<b>Số nguyên tố đầu tiên của mảng:</b> ${primeNumber}`;
  } else {
    resultInnerEl.innerHTML = `Không có số nguyên tố nào trong mảng`;
  }
}
